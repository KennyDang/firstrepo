<?php
namespace App\Controller;

use App\Controller\AppController;

class PicsController extends AppController
{
	public function index()
    {
        $pics = $this->paginate($this->Pics);

        $this->set(compact('pics'));
        $this->set('_serialize', ['pics']);
    }

    public function add()
    {
        $pic = $this->Pics->newEntity();
        if ($this->request->is('post')) {
            $pic = $this->Pics->patchEntity($pic, $this->request->data);
            if ($this->Pics->save($pic)) {
                $this->Flash->success(__('The file has been uploaded.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The file could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pic'));
        $this->set('_serialize', ['pic']);
    }

    public function view($id = null)
    {
        $pic = $this->Pics->get($id, [
            'contain' => []
        ]);

        $this->set('pic', $pic);
        $this->set('_serialize', ['pic']);
    }
}