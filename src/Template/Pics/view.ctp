<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('File list'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<h1><?= h($pic->id) ?></h1>
<div>
<?php
    // You could use the following to create a link to 
    // the image (with default settings in place of course)
    echo $this->Html->image('/webroot/files/Pics/photo/' . $pic->photo, [
    		"alt" => $pic->photo,
    		"width" => "200",
    		"height" => "200",
    		"style" => "position:absolute"
    	]);
    echo $this->Html->link('/webroot/files/Pics/photo/' . $pic->photo);
    //
 ?>
</div>
<div>
<p><?= h($pic->description) ?></p>
</div>