
<h1>Uploaded File List</h1>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Upload file'), ['action' => 'add']) ?></li>
    </ul>
</nav>

<table>
    <tr>
        <th>Id</th>
        <th>Photo Name</th>
        <th>Description</th>
        <th>Dir</th>
    </tr>

    <!-- Here is where we iterate through our $articles query object, printing out article info -->

    <?php foreach ($pics as $pic): ?>
    <tr>
        <td><?= $pic->id ?></td>
        <td>
            <?= $this->Html->link($pic->photo, ['action' => 'view', $pic->id]) ?>
        </td>
        <td>
            <?= $pic->description ?>
        </td>
        <td>
            <?= $pic->photo_dir ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>