<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Players'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="players form large-9 medium-8 columns content">
    <?= $this->Form->create($player) ?>
    <fieldset>
        <legend><?= __('Add Player') ?></legend>
        <?php
            echo $this->Form->input('f_name');
            echo $this->Form->input('l_name');
            echo $this->Form->input('gender');
            echo $this->Form->input('dob');
            echo $this->Form->input('phone');
            echo $this->Form->input('address');
            echo $this->Form->input('email');
            echo $this->Form->input('season_fee');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
