<!DOCTYPE html>
<html> 
	<head>
		<title>Pure CSS Responsive Accordion Table</title>
		
		<<?php echo $this->Html->css('blogtable.css'); ?>

	</head>
	<body>
		<div id="table">
			<div class="header-row row">
			    <span class="cell primary">Vehcile</span>
			    <span class="cell">Exterior</span>
			    <span class="cell">Interior</span>
			    <span class="cell">Engine</span>
			    <span class="cell">Trans</span>
		  	</div>

			<div class="row">
				<input type="radio" name="expand">
			    <span class="cell primary" data-label="Vehicle">2013 Subaru WRX</span>
			    <span class="cell" data-label="Exterior">World Rally Blue</span>
			    <span class="cell" data-label="Interior">Black</span>
			    <span class="cell" data-label="Engine">2.5L H4 Turbo</span>
			    <span class="cell" data-label="Trans"><a href="">5 Speed</a></span>
			</div>

		  <div class="row">
				<input type="radio" name="expand">
			    <span class="cell primary" data-label="Vehicle">2013 Subaru WRX STI</span>
			    <span class="cell" data-label="Exterior">Crystal Black Silica</span>
			    <span class="cell" data-label="Interior">Black</span>
			    <span class="cell" data-label="Engine">2.5L H4 Turbo</span>
			    <span class="cell" data-label="Trans">6 Speed</span>
		  </div>

		  <div class="row">
				<input type="radio" name="expand">
			    <span class="cell primary" data-label="Vehicle">2013 Subaru BRZ</span>
			    <span class="cell" data-label="Exterior">Dark Grey Metallic</span>
			    <span class="cell" data-label="Interior">Black</span>
			    <span class="cell" data-label="Engine">2.0L H4</span>
			    <span class="cell" data-label="Trans">6 Speed</span>
		  </div>

	</div>

		<!-- <?php
		    echo $this->Html->image("homepage1.jpg", [
		    	"width" => "200",
		        "height" => "200"]);
		    echo $this->Html->image("homepage2.jpg", [
		    	"width" => "200",
		        "height" => "200"]);
		    echo $this->Html->image("homepage3.jpg", [
		    	"width" => "200",
		        "height" => "200"]);
		?> -->
	</body>
</html>