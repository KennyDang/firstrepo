<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Player Entity
 *
 * @property int $id
 * @property string $f_name
 * @property string $l_name
 * @property string $gender
 * @property \Cake\I18n\Time $dob
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property int $season_fee
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 */
class Player extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
