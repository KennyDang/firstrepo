<?php
    namespace App\Model\Table;

    use Cake\ORM\Table;
    use Cake\ORM\Query;

    class PicsTable extends Table
    {
        public function initialize(array $config)
        {
            $this->table('pics');
            $this->displayField('photo');
            $this->primaryKey('id');
            $this->addBehavior('Josegonzalez/Upload.Upload', [
                'photo' => [
                    'fields' => [
                        // if these fields or their defaults exist
                        // the values will be set.
                        'dir' => 'photo_dir', // defaults to `dir`
                    ],
                ],
            ]);
        }
    }
    ?>
